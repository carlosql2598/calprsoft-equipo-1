﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GestionReparacion.CapaDominio.Entidades;

namespace GestionReparacion.UnitTestCapaDominio
{
    [TestClass]
    public class ReparacionUnitTest
    {
        [TestMethod]
        public void esFechaEntregaValidaTestMethod1()
        {
            Reparacion reparacion = new Reparacion();

            var date = DateTime.Today.AddDays(1);
            reparacion.FechaFinReparacion = date;

            Assert.IsTrue(reparacion.esFechaEntregaValida());
        }

        [TestMethod]
        public void esValidoCostoTotalTestMethod2()
        {
            Reparacion reparacion = new Reparacion();

            reparacion.CostoReparacion = 42;

            Assert.IsTrue(reparacion.esValidoCostoTotal());

        }
    }

}