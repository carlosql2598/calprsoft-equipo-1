﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GestionReparacion.CapaDominio.Entidades;

namespace GestionReparacion.UnitTestCapaDominio
{
    [TestClass]
    public class CajaComponentesUnitTest
    {
        [TestMethod]
        public void calcularCostoTotalMethod1()
        {
            Componente componente1 = new Componente();
            Componente componente2 = new Componente();

            double valorEsperado = 110;
            componente1.IdComponente = 1;
            componente1.Marca = "MARCA";
            componente1.NombreComponente = "LENOVO";
            componente1.Stock = 30;
            componente1.Precio = 40;
            componente1.Tipo = "qde";

            componente2.IdComponente = 2;
            componente2.Marca = "MARCA 2";
            componente2.NombreComponente = "ASUS";
            componente2.Stock = 30;
            componente2.Precio = 70;
            componente2.Tipo = "VSJD";


            CajaComponentes cajaComponentes = new CajaComponentes();
            cajaComponentes.Componentes.Insert(0, componente1);
            cajaComponentes.Componentes.Insert(1, componente2);
            Assert.AreEqual(valorEsperado, cajaComponentes.calcularCostoTotal());
        }

    }
}
