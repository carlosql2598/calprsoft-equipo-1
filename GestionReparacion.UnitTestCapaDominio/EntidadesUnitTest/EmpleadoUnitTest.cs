﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GestionReparacion.CapaDominio.Entidades;

namespace GestionReparacion.UnitTestCapaDominio
{
    [TestClass]
    public class EmpleadoUnitTest
    {
        [TestMethod]
        public void esCambiableEstadoTestMethod1()
        {
            Reparacion reparacion = new Reparacion();
            reparacion.FechaFinReparacion = DateTime.Today.AddDays(1);

            Empleado empleado = new Empleado();
            empleado.reparacion = reparacion;

            bool resultadoEsperado = false;

            Assert.AreEqual(empleado.esCambiableEstado(), resultadoEsperado);
        }

        [TestMethod]
        public void esEdadAceptableTestMethod1()
        {
            Empleado empleado = new Empleado();

            empleado.Edad = 25;

            Assert.IsTrue(empleado.esEdadAceptable());
        }

        [TestMethod]
        public void esLibreTestMethod1()
        {
            Empleado empleado = new Empleado();
            empleado.EstadoDisponible = true;

            bool resultadoEsperado = true;

            Assert.AreEqual(empleado.esLibre(), resultadoEsperado);
        }

        [TestMethod]
        public void cambiarEstadoEmpleadoTestMethod1()
        {
            Reparacion reparacion = new Reparacion();
            reparacion.FechaFinReparacion = DateTime.Now;

            Empleado empleado = new Empleado();
            empleado.reparacion = reparacion;

            empleado.cambiarEstadoEmpleado();
            bool resultadoEsperado = true;

            Assert.AreEqual(empleado.EstadoDisponible, resultadoEsperado);
        }
    }
}
