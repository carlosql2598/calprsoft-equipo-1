﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GestionReparacion.CapaDominio.Entidades;

namespace GestionReparacion.UnitTestCapaDominio
{
    [TestClass]
    public class ComponetesUnitTest
    {
        [TestMethod]
        public void esValidoStockTestMethod1()
        {
            Componente componente = new Componente();

            int cantidad = 2;
            componente.Stock = 1;

            Boolean resultadoEsperado = false;

            Assert.AreEqual(componente.esValidoStock(cantidad), resultadoEsperado);
        }

        [TestMethod]
        public void actualizarStockTestMethod2()
        {
            Componente componente = new Componente();
            componente.Stock = 5;
            int cantidad = 3;

            Assert.IsTrue(componente.actualizarStock(cantidad));
        }

    }
}
