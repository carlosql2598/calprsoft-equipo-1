﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using GestionReparacion.CapaDominio.Contratos;
using GestionReparacion.CapaDominio.Entidades;

namespace GestionReparacion.CapaPersistencia.SQLServerDAO
{
    public class ComponenteDao : IComponenteDao
    {
        private GestorDAO gestorDAO;

        public ComponenteDao(IGestorDAO gestorDAO)
        {
            this.gestorDAO = (GestorDAO)gestorDAO;
        }

        public void actualizarComponte(Componente componente)
        {
            string actualizarComponenteSQL;

            actualizarComponenteSQL = "UPDATE calidad.Componentes SET " +
                                        "nombre = @nombre, " +
                                        "precio = @precio, " +
                                        "tipo = @tipo, " +
                                        "marca = @marca" +
                                        "stock = @stock" +
                                        "WHERE idComponente = @idComponente";
            try
            {
                SqlCommand comando;

                comando = gestorDAO.obtenerComandoSQL(actualizarComponenteSQL);

                comando.Parameters.AddWithValue("@nombre", componente.NombreComponente);
                comando.Parameters.AddWithValue("@precio", componente.Precio);
                comando.Parameters.AddWithValue("@tipo", componente.Tipo);
                comando.Parameters.AddWithValue("@marca", componente.Marca);
                comando.Parameters.AddWithValue("@stock", componente.Stock);
                comando.Parameters.AddWithValue("@idComponente", componente.IdComponente);

                comando.ExecuteNonQuery();

            }
            catch (Exception err)
            {
                throw new Exception("Ocurrio un problema al intentar actualizar el componente", err);
            }
        }


    }
}