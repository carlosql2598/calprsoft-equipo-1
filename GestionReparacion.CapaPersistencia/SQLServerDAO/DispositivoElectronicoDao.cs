﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using GestionReparacion.CapaDominio.Contratos;
using GestionReparacion.CapaDominio.Entidades;

namespace GestionReparacion.CapaPersistencia.SQLServerDAO
{
    public class DispositivoElectronicoDao : IDispositivoElectronicoDao
    {
        private GestorDAO gestorDAO;

        public DispositivoElectronicoDao(IGestorDAO gestorDAO)
        {
            this.gestorDAO = (GestorDAO)gestorDAO;
        }

        public void guardarDispositivo(DispositivoElectronico dispositivoElectrinco)
        {
            string insertarDispositivoSql;

            insertarDispositivoSql = "insert into calidad.DispositivoElectronico(nombreDispositivo, modelo, marca, estadoDispositivo) " +
                                     "values(@nombreDispositivo, @modelo, @marca, @estadoDispositivo)";

            try
            {
                SqlCommand comando;

                comando = gestorDAO.obtenerComandoSQL(insertarDispositivoSql);

                //comando.Parameters.AddWithValue("@idCuota_PK_FK", dispositivoElectrinco.Cuota.IdCuota);
                comando.Parameters.AddWithValue("@nombreDispositivo", dispositivoElectrinco.NombreDispositivo);
                comando.Parameters.AddWithValue("@modelo", dispositivoElectrinco.Modelo);
                comando.Parameters.AddWithValue("@marca", dispositivoElectrinco.Marca);
                comando.Parameters.AddWithValue("@estadoDispositivo", dispositivoElectrinco.EstadoDispositivo);

                comando.ExecuteNonQuery();

            }
            catch (Exception err)
            {
                throw new Exception("Ocurrio un problema al intentar guardar el dispositivo", err);
            }
        }

        public void actualizarDispositivo(DispositivoElectronico dispositivoElectronico)
        {
            string actualizarDispositivoQL;

            actualizarDispositivoQL = "UPDATE calidad.DispositivoElectronico SET " +
                                        "nombreDispositivo = @nombreDispositivo, " +
                                        "estadoDispositivo = @estadoDispositivo, " +
                                        "modelo = @modelo, " +
                                        "marca= @marca" +
                                        "WHERE idDispositivoElectronico = @idDispositivoElectronico";
            try
            {
                SqlCommand comando;

                comando = gestorDAO.obtenerComandoSQL(actualizarDispositivoQL);

                comando.Parameters.AddWithValue("@nombreDispositivo", dispositivoElectronico.NombreDispositivo);
                comando.Parameters.AddWithValue("@estadoDispositivo", dispositivoElectronico.EstadoDispositivo);
                comando.Parameters.AddWithValue("@modelo", dispositivoElectronico.Modelo);
                comando.Parameters.AddWithValue("@marca", dispositivoElectronico.Marca);
                comando.Parameters.AddWithValue("@idDispositivoElectronico", dispositivoElectronico.IdDispositivo);

                comando.ExecuteNonQuery();

            }
            catch (Exception err)
            {
                throw new Exception("Ocurrio un problema al intentar actualizar el dispositivo electronico", err);
            }
        }


    }
}
