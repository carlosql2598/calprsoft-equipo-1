﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using GestionReparacion.CapaDominio.Contratos;
using GestionReparacion.CapaDominio.Entidades;

namespace GestionReparacion.CapaPersistencia.SQLServerDAO
{
    public class EmpleadoDao : IEmpleadoDao
    {
        private GestorDAO gestorDAO;

        public EmpleadoDao(IGestorDAO gestorDAO)
        {
            this.gestorDAO = (GestorDAO)gestorDAO;
        }

        public void guardarEmpleado(Empleado empleado)
        {
            // Si es que se hace en este caso se tiene que hacer inserción en dos tablas, 
            // se creará un procedimiento 
        }

        public void actualizarEmpleado(Empleado empleado)
        {
            // Aca solo se actualiza el estado, deberia ser todo

            string actualizarEmpleadoSQL;

            actualizarEmpleadoSQL = "UPDATE calidad.Empleado_Reparador SET " +
                                        "estadoDisponible = @estadoDisponible, " +
                                        "WHERE idPersona = @idPersona";
            try
            {
                SqlCommand comando;

                comando = gestorDAO.obtenerComandoSQL(actualizarEmpleadoSQL);

                comando.Parameters.AddWithValue("@estadoDisponible", empleado.EstadoDisponible);
                comando.Parameters.AddWithValue("@idPersona", empleado.IdPersona);

                comando.ExecuteNonQuery();

            }
            catch (Exception err)
            {
                throw new Exception("Ocurrio un problema al intentar actualizar el Empleado", err);
            }
        }
    }
}
