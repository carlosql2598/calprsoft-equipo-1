﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using GestionReparacion.CapaDominio.Contratos;
using GestionReparacion.CapaDominio.Entidades;

namespace GestionReparacion.CapaPersistencia.SQLServerDAO
{
    public class ReparacionDao : IReparacionDao
    {
        private GestorDAO gestorDAO;

        public ReparacionDao(IGestorDAO gestorDAO)
        {
            this.gestorDAO = (GestorDAO)gestorDAO;
        }

        public void guardarReparacion(ComprobanteDePago reparacion)
        {
            string insertarReparacionSQL;

            insertarReparacionSQL = "insert into calidad.Reparacion(fechaInicioReparacion, fechaFinReparacion, " +
                                                                    "descripcionDelProblema, costoDeReparacion) " +
                                    "values(@fechaInicioReparacion, @fechaFinReparacion, @descripcionDelProblema," +
                                                                   "@costoDeReparacion)";
            try
            {
                SqlCommand comando;

                comando = gestorDAO.obtenerComandoSQL(insertarReparacionSQL);

                comando.Parameters.AddWithValue("@fechaInicioReparacion", reparacion.FechaInicioReparacion);
                comando.Parameters.AddWithValue("@fechaFinReparacion", reparacion.FechaFinReparacion);
                comando.Parameters.AddWithValue("@descripcionDelProblema", reparacion.DescripcionProblema);
                comando.Parameters.AddWithValue("@costoDeReparacion", reparacion.CostoReparacion);

                comando.ExecuteNonQuery();

            }
            catch (Exception err)
            {
                throw new Exception("Ocurrio un problema al intentar guardar la reparacion", err);
            }

        }

        public void actualizarReparacion(ComprobanteDePago reparacion)
        {
            string actualizarReparacionSQL;

            actualizarReparacionSQL = "UPDATE calidad.Reparacion SET " +
                                        "fechaInicioReparacion = @fechaInicioReparacion, " +
                                        "fechaFinReparacion = @fechaFinReparacion, " +
                                        "descripcionDelProblema = @descripcionDelProblema, " +
                                        "costoDeReparacion = @costoDeReparacion" +
                                        "WHERE idReparacion = @idReparacion";
            try
            {
                SqlCommand comando;

                comando = gestorDAO.obtenerComandoSQL(actualizarReparacionSQL);

                comando.Parameters.AddWithValue("@fechaInicioReparacion", reparacion.FechaInicioReparacion);
                comando.Parameters.AddWithValue("@fechaFinReparacion", reparacion.FechaFinReparacion);
                comando.Parameters.AddWithValue("@descripcionDelProblema", reparacion.DescripcionProblema);
                comando.Parameters.AddWithValue("@costoDeReparacion", reparacion.CostoReparacion);
                comando.Parameters.AddWithValue("@idReparacion", reparacion.Id);

                comando.ExecuteNonQuery();

            }
            catch (Exception err)
            {
                throw new Exception("Ocurrio un problema al intentar actualizar la reparacion", err);
            }

        }
         
    }
}
