﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionReparacion.CapaDominio.Entidades
{
    public class PedidoComponente
    {
        private int idCajaComponentes;
        private int cantidadComponentes;
        //private double costoTotalComponentes = 0.0;
        //private List<Componente> componentes;
        private Componente componente;

        public PedidoComponente()
        {
            //componentes = new List<Componente>();
            componente = new Componente();
        }

        public int IdCajaComponentes { get => idCajaComponentes; set => idCajaComponentes = value; }
        public int CantidadComponentes { get => cantidadComponentes; set => cantidadComponentes = value; }
        //public double CostoTotalComponentes { get => costoTotalComponentes; set => costoTotalComponentes = value; }
        public Componente Componente { get => componente; set => componente = value; }

        public double calcularDescuentoPorMesFestivo()
        {
            double descuento = 0.0;
            DateTime fechaActual = DateTime.Today;
            string mesActual = fechaActual.ToString("MMMM").ToLower();

            if(mesActual == "febrero" || mesActual == "julio" || mesActual == "diciembre")
            {
                descuento = 0.05 * calcularMontoTotal();
            }

            return descuento;
        }

        public double calcularDescuentoPorCantidad()
        {
            double descuento = 0.0;

            if (cantidadComponentes > 10)
            {
                descuento = 0.1 * calcularMontoTotal();
            }

            return descuento;
        }

        public double calcularDescuentoTotal()
        {
            return calcularDescuentoPorCantidad() + calcularDescuentoPorMesFestivo();
        }

        public double calcularCostoTotal()
        {
            return calcularMontoTotal() - calcularDescuentoTotal();
        }

        public double calcularMontoTotal()
        {
            return componente.Precio * cantidadComponentes;
        }

        public Boolean esValidoStock()
        {
            return cantidadComponentes >= componente.Stock;
        }

        public Boolean actualizarStock()
        {
            if (esValidoStock())
            {
                componente.Stock -= cantidadComponentes;

                return true;
            }

            return false;
        }
    }
}
