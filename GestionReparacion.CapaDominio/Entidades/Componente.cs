﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionReparacion.CapaDominio.Entidades
{
    public class Componente
    {

        private int idComponente;
        private String nombreComponente;
        private String tipo;
        private String marca;
        private float precio;
        private int stock;
        private int dificultad;

        public int IdComponente { get => idComponente; set => idComponente = value; }
        public string NombreComponente { get => nombreComponente; set => nombreComponente = value; }
        public string Tipo { get => tipo; set => tipo = value; }
        public string Marca { get => marca; set => marca = value; }
        public float Precio { get => precio; set => precio = value; }
        public int Stock { get => stock; set => stock = value; }
        public int Dificultad { get => dificultad; set => dificultad = value; }

        public bool estaDisponible()
        {
            return stock > 0;
        }
        
    }
}
