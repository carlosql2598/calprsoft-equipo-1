﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionReparacion.CapaDominio.Entidades
{
    public class DispositivoElectronico
    {
        private int idDispositivo;
        private String nombreDispositivo;
        private String modelo;
        private String marca;
        private Boolean estadoDispositivo;

        public int IdDispositivo { get => idDispositivo; set => idDispositivo = value; }
        public string NombreDispositivo { get => nombreDispositivo; set => nombreDispositivo = value; }
        public string Modelo { get => modelo; set => modelo = value; }
        public string Marca { get => marca; set => marca = value; }
        public bool EstadoDispositivo { get => estadoDispositivo; set => estadoDispositivo = value; }
    }
}
