﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionReparacion.CapaDominio.Entidades
{
    public class ComprobanteDePago
    {

        private int id;
        private bool esServicioDeEnsamblajeAceptado;

        private Empleado empleado;
        private List<PedidoComponente> listaPedidoComponentes = new List<PedidoComponente>();

        public int Id { get => id; }

        public double calcularMontoTotal()
        {
            double montoTotal = 0.0;
            double costoPorDelivery = 8;

            foreach (var item in listaPedidoComponentes)
            {
                montoTotal += item.calcularCostoTotal();
            }

            return montoTotal + costoPorDelivery + calcularCostoDeEnsamblado();
        }

        public double calcularCostoDeEnsamblado()
        {
            double costoEnsamblado = 0.0;

            if(esServicioDeEnsamblajeAceptado)
            {
                foreach (var item in listaPedidoComponentes)
                {
                    costoEnsamblado += (item.Componente.Dificultad / 100) * item.Componente.Precio;
                }
            }

            return costoEnsamblado;
        }
        /*
        public Boolean esFechaEntregaValida()
        {
            return DateTime.Now <= fechaFinReparacion;
        }

        public Boolean esValidoCostoTotal()
        {
            return (costoReparacion >= 20 && costoReparacion <= 60);
        }

        
        public void cambiarEstadoEmpleado()
        {
            if (empleado.esCambiableEstado())
            {
                empleado.EstadoDisponible = true;
            }
            else
            {
                empleado.EstadoDisponible = false;
            }
        }*/
    }
}
