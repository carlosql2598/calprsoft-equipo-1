﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GestionReparacion.CapaDominio.Contratos;
using GestionReparacion.CapaDominio.Entidades;
using GestionReparacion.CapaPersistencia.SQLServerDAO;

namespace GestionReparacion.CapaAplicacion.Servicios
{
    class GestionarReparacionDispositivoService
    {
        private IClienteDao clienteDao;
        private IComponenteDao componenteDao;
        private IDispositivoElectronicoDao dispositivoElectronicoDao;
        private IEmpleadoDao empleadoDao;
        private IGestorDAO gestorDAO;
        private IPersonaDao personaDao;
        private IReparacionDao reparacionDao;

        public GestionarReparacionDispositivoService()
        {
            gestorDAO = new GestorDAO();
            clienteDao = new ClienteDao(gestorDAO);
            componenteDao = new ComponenteDao(gestorDAO);
            dispositivoElectronicoDao = new DispositivoElectronicoDao(gestorDAO);
            empleadoDao = new EmpleadoDao(gestorDAO);
            personaDao = new PersonaDao(gestorDAO);
            reparacionDao = new ReparacionDao(gestorDAO);
        }

        /////////////// CAJA COMPONENTES /////////////////
        
        public double obtenerCostoTotal(PedidoComponente cajaComponentes)
        {
            return cajaComponentes.calcularCostoTotal();
        }

        /////////////// COMPONENTES /////////////////
        
        public void actualizarStock(Componente componente, int cantidad)
        {

            if (componente.actualizarStock(cantidad))
            {
                gestorDAO.abrirConexion();
                componenteDao.actualizarComponte(componente);
                gestorDAO.cerrarConexion();
            }

            else
            {
                throw new Exception("Stock no valido");
            }  
        }

        /////////////// EMEPLADO /////////////////
        
        public Boolean esCambiableEstado (Empleado empleado)
        {
            return empleado.esCambiableEstado();
        }

        public Boolean esEdadAceptable(Empleado empleado)
        {
            return empleado.esEdadAceptable();
        }

        public Boolean esLibre(Empleado empleado)
        {
            return empleado.esLibre();
        }

        public void cambiarEstadoEmpleado(Empleado empleado)
        {
            gestorDAO.abrirConexion();
            empleado.cambiarEstadoEmpleado();
            //reparacionDao.actualizarReparacion(reparacion);
            empleadoDao.actualizarEmpleado(empleado);
            gestorDAO.cerrarConexion();
        }

        /////////////// REPARACION /////////////////

        public Boolean validarReparacion (ComprobanteDePago reparacion)
        {
            if(!reparacion.esFechaEntregaValida())
            {
                throw new Exception("Fecha de entrega no valida");
            }

            if (!reparacion.esValidoCostoTotal())
            {
                throw new Exception("El costo total no es valido");
            }

            return true;
        }

        /*public void cambiarEstadoEmpleado(Reparacion reparacion)
        {
            gestorDAO.abrirConexion();
            reparacion.cambiarEstadoEmpleado();
            reparacionDao.actualizarReparacion(reparacion);
            gestorDAO.cerrarConexion();
        }
        */
    }
}
